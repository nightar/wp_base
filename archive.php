<?php

use theme\Helper;

get_header();
$content_attributes = [
  'id'    => 'content',
  'class' => [
    'container',
  ]
];
if ( is_active_sidebar( 'default-sidebar' ) ) {
  $content_attributes[ 'class' ][] = 'has-sidebar';
}; ?>
  <div<?php echo Helper::getAttrString( $content_attributes ); ?>>
    <div class="posts-holder">
      <div class="holder">
        <?php the_archive_title( '<h2>', '</h2>' ); ?>
        <?php if ( have_posts() ) : ?>
          <div class="list">
            <?php while ( have_posts() ) : the_post(); ?>
              <?php do_action( 'location-blocks/archive/content' ); ?>
            <?php endwhile; ?>
            <?php do_action( 'location-blocks/archive/pager' ); ?>
          </div>
        <?php else : ?>
          <?php do_action( 'location-blocks/not_found' ); ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>