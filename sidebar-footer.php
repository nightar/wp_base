<?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
	<section class="footer-sidebar">
		<?php dynamic_sidebar( 'footer-sidebar' ); ?>
	</section>
<?php endif; ?>