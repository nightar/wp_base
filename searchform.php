<?php
use theme\Helper;
use theme\Theme;
$search_text = Helper::buffer( 'search_text', null, true);
$sq = get_search_query() ? get_search_query() : $search_text; ?>
<form method="get" class="search-form" action="<?php echo home_url(); ?>" >
	<fieldset>
		<input type="search" name="s" placeholder="<?php echo $sq; ?>" value="<?php echo get_search_query(); ?>" />
		<button class="submit" type="submit"><i class="icon-search"></i><span><?php _e( 'Search', Theme::domain() ); ?></span></button>
	</fieldset>
</form>