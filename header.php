<?php
use theme\Helper;
$header_attributes = [
  'class' => [
    'header',
    'section-header',
    Helper::getOption('sticky-header') ? 'sticky' : '',
  ],
  'id' => 'header',
];
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="<?php bloginfo('charset'); ?>">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper">
    <header<?php echo Helper::getAttrString($header_attributes); ?>>
      <?php do_action('location-blocks/section/header'); ?>
    </header>
    <main role="main" id="main">
      <?php do_action('location-blocks/section/banner'); ?>
      <?php do_action('location-blocks/section/breadcrambs'); ?>
        <div class="content-wrapper">