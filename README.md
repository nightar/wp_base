# WordPress started theme
***
This is started theme for CMS WordPress. 
##### Base settings of theme including:
 - cusomize logo. 
 - sticky header.
 - customize social links.

All settings can be changes from options page.
***
### Install
##### Via composer
 - Install [Composer][composer]( pass this step, if composer is installed)
 - Go to the folder with wordpress themes
 - Open console
 - Type 
```sh
composer create-project nightar/wp_base
```
 - Rename folder and use them

##### Manually

 - Download project from [gitlab][gitlab]
 - Put started theme to wordpress themes folder
 - Rename folder and use them
***
### Options

|Property|Description|  
|---|:---:|
|Platform|CMS WordPress|
|Design|1-column, 2-column, responsive, sticky-header|
|Theme Options|yes|
|Template|PHP|
|Requirements|php v5.6+|
    

For more infromations, you can contact with me by [email][mail] 
***
[mail]: <mailto:syallin@gmail.com>
[composer]: https://getcomposer.org/doc/00-intro.md "composer"
[gitlab]: https://gitlab.com/nightar/wp_base "gitlab"