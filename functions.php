<?php
use theme\Theme;
$autoloader = get_stylesheet_directory() . DIRECTORY_SEPARATOR . 'inc' . DIRECTORY_SEPARATOR . 'Autoloader.php';

if ( file_exists( $autoloader ) ) {
  include( $autoloader );
}
try {
  Theme::instance();
} catch ( Exception $e ) {
}