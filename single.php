<?php
use theme\Helper;

get_header();
$content_attributes = [
  'id' => 'content',
];
?>
  <div<?php echo Helper::getAttrString( $content_attributes ); ?>>
    <?php while( have_posts() ) : the_post(); ?>
    <div class="content-wrap">
      <div class="holder">
          <?php do_action( 'location-blocks/single/content' ); ?>
          <?php comments_template(); ?>
      </div>
    </div>
    <?php endwhile; ?>
  </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>