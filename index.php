<?php

use theme\Helper;

get_header();
$content_attributes = [
  'id'    => 'content',
  'class' => [
    'container',
  ]
];
if ( is_active_sidebar( 'default-sidebar' ) ) {
  $content_attributes[ 'class' ][] = 'has-sidebar';
};?>
  <div<?php echo Helper::getAttrString( $content_attributes ); ?>>
  </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>