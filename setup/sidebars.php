<?php
use theme\Theme;
$sidebars = [
  'default-sidebar' => [
    'id'            => 'default-sidebar',
    'name'          => __( 'Default Sidebar', Theme::domain() ),
    'before_widget' => '<div class="widget %2$s" id="%1$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ],'footer-sidebar' => [
    'id'            => 'footer-sidebar',
    'name'          => __( 'Footer Sidebar', Theme::domain() ),
    'before_widget' => '<div class="widget %2$s" id="%1$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
  ],
];