<?php

use theme\Helper;
use theme\Theme;

/**
 *  Theme styles
 */
if ( Helper::getOption( 'react-enable' ) ) {
  $styles = [];
} else {
  $styles = [
    Theme::domain() . '-style' => [
      'src' => get_stylesheet_uri(),
      'deps' => ['dashicons'],
      'version' => Theme::version(),
      'media' => 'all',
      'rewrite' => false,
      'rule' => '',
    ],
  ];
}