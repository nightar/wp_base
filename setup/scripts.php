<?php
use theme\Theme;
use theme\Helper;

/**
 * Theme scripts
 *
 * $parametrs = [
 * 'obj_name'     => 'theme_data',
 * 'ajax_url'     => add_query_arg( [ 'action' => '%%endpoint%%' ], admin_url( 'admin-ajax.php' ) ),
 * 'scripts_path' => get_template_directory() . '/js/',
 * 'scripts_uri'  => get_template_directory_uri() . '/js/',
 * 'scripts'      => [],
 * ];
 * See 'theme-script-parameters' filter
 *
 * $in_footer = true by default. See 'scripts-footer' filter
 */
if ( Helper::getOption( 'react-enable' ) ) {
  $scripts = [
    Theme::domain() . '-react-script'       => [
      'src'        => get_template_directory_uri() . '/js/react.main.js',
      'deps'       => [ 'jquery' ],
      'version'    => Theme::version(),
      'footer'     => $in_footer,
      'rewrite'    => false,
      'rule'       => 'react_enable',
      'parameters' => $parametrs,
    ],
  ];
} else {
  $scripts = [
    'comment-reply' => [
      'src' => get_template_directory_uri() . '/js/comment-reply.js',
      'deps' => [],
      'version' => Theme::version(),
      'footer' => $in_footer,
      'rewrite' => true,
      'rule' => 'can_comment',
    ],
    Theme::domain() . '-script' => [
      'src' => get_template_directory_uri() . '/js/jquery.main.js',
      'deps' => ['jquery'],
      'version' => Theme::version(),
      'footer' => $in_footer,
      'rewrite' => false,
      'rule' => '',
      'parameters' => $parametrs,
    ],
    Theme::domain() . '-theme-script' => [
      'src' => get_template_directory_uri() . '/js/theme.js',
      'deps' => ['jquery', Theme::domain() . '-script',],
      'version' => Theme::version(),
      'footer' => $in_footer,
      'rewrite' => false,
      'rule' => '',
      'parameters' => $parametrs,
    ],
  ];
}