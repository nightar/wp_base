<?php
$plugins = [
  [
    'name'     => 'Advanced Custom Fields',
    'slug'     => 'advanced-custom-fields-pro',
    'source'   => 'advanced-custom-fields-pro.5.8.0-beta3.zip',
    'required' => false,
    'version'  => '5.8.0-beta3',
  ]
];