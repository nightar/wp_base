<?php
/**
 * Dummies for ACF plugin functions
 */
if ( ! class_exists( 'acf' ) && ! is_admin() ) {
  function get_field( $selector, $post_id = false, $format_value = true )  { return ''; }
  function the_field( $selector, $post_id = false, $format_value = true ) {}
  function get_field_object( $selector, $post_id = false, $format_value = true, $load_value = true ) { return []; }
  function get_fields( $post_id = false, $format_value = true ) { return []; }
  function get_field_objects( $post_id = false, $format_value = true, $load_value = true ) { return []; }
  function have_rows( $selector, $post_id = false ) { return false; }
  function the_row( $format = false ) {}
  function get_row( $format = false ) { return false; }
  function get_row_index() { return false; }
  function get_row_sub_field( $selector ) { return []; }
  function get_row_sub_value( $selector ) { return false; }
  function reset_rows() { return false; }
  function has_sub_field( $field_name, $post_id = false ) { return false; }
  function has_sub_fields( $field_name, $post_id = false ) { return false; }
  function get_sub_field( $selector = '', $format_value = true ) { return false; }
  function the_sub_field( $field_name, $format_value = true ) {}
  function get_sub_field_object( $selector, $format_value = true, $load_value = true ) { return [];}
  function get_row_layout() { return false; }
  function acf_shortcode( $atts ) { return ''; }
  function update_field( $selector, $value, $post_id = false ) { return false; }
  function update_sub_field( $selector, $value, $post_id = false ) { return false; }
  function delete_field( $selector, $post_id = false ) { return false; }
  function delete_sub_field( $selector, $post_id = false ) { return false; }
  function add_row( $selector, $row = false, $post_id = false ) { return false; }
  function add_sub_row( $selector, $row = false, $post_id = false ) { return false; }
  function update_row( $selector, $i = 1, $row = false, $post_id = false ) { return false; }
  function update_sub_row( $selector, $i = 1, $row = false, $post_id = false ) { return false; }
  function delete_row( $selector, $i = 1, $post_id = false ) { return false; }
  function delete_sub_row( $selector, $i = 1, $post_id = false ) { return false; }
  function create_field( $field ) {}
  function render_field( $field ) {}
  function reset_the_repeater_field() { return false; }
  function the_repeater_field( $field_name, $post_id = false ) { return false; }
  function the_flexible_field( $field_name, $post_id = false ) { return false; }
  function acf_filter_post_id( $post_id ) { return false; }
}