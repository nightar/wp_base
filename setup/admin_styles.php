<?php
use theme\Theme;


$styles = [
  Theme::domain() . '-admin-style' => [
    'src'     => get_template_directory_uri() . '/admin.css',
    'deps'    => [],
    'version' => Theme::version(),
    'media'   => 'all',
    'rewrite' => false,
    'rule'    => '',
  ],
];