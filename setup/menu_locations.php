<?php

use theme\Theme;

$menu_list = [
  'primary' => __( 'Primary Navigation', Theme::domain() ),
  'footer'  => __( 'Footer Navigation', Theme::domain() )
];