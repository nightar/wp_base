<?php

use theme\Theme;

$options = [
  [
    'title'  => __( 'Generals', Theme::domain() ),
    'id'     => 'generals',
    'desc'   => __( 'General options', Theme::domain() ),
    'fields' => []
  ],
  [
    'title'      => __( 'Header', Theme::domain() ),
    'id'         => 'generals-header',
    'subsection' => true,
    'fields'     => [
      [
        'id'      => 'sticky-header',
        'type'    => 'switch',
        'title'   => __( 'Sticky header', Theme::domain() ),
        'default' => false,
      ],
    ],
  ],
  [
    'title'      => __( 'Logo', Theme::domain() ),
    'id'         => 'generals-logo',
    'subsection' => true,
    'desc'       => __( 'Theme logotypes', Theme::domain() ),
    'fields'     => [
      [
        'id'    => 'primary-logo',
        'type'  => 'media',
        'title' => __( 'Primary logo', Theme::domain() ),
        'desc'  => __( 'Primary theme logo', Theme::domain() ),
      ],
      [
        'id'    => 'secondary-logo',
        'type'  => 'media',
        'title' => __( 'Secondary logo', Theme::domain() ),
        'desc'  => __( 'Secondary theme logo', Theme::domain() ),
      ],
      [
        'id'    => 'footer-logo',
        'type'  => 'media',
        'title' => __( 'Footer logo', Theme::domain() ),
        'desc'  => __( 'Footer theme logo', Theme::domain() ),
      ],
    ]
  ],
  [
    'title'      => __( 'Socials', Theme::domain() ),
    'id'         => 'generla-socials',
    'subsection' => true,
    'desc'       => __( 'Theme Socials', Theme::domain() ),
    'fields'     => [
      [
        'id'    => 'socials',
        'type'  => 'social_links',
        'title' => __( 'Social links', Theme::domain() ),
      ],
    ]
  ],
  [
    'title'  => __( 'Content', Theme::domain() ),
    'id'     => 'general-content',
    'subsection' => true,
    'fields' => [
    ]
  ],
  [
    'title'  => __( 'Footer', Theme::domain() ),
    'id'     => 'general-footer',
    'subsection' => true,
    'fields' => [
      [
        'id'    => 'copyright',
        'type'  => 'text',
        'title' => __( 'Copyright', Theme::domain() ),
      ],
    ]
  ],
  [
    'title'  => __( 'React', Theme::domain() ),
    'id'     => 'react',
    'desc'   => __( 'React options', Theme::domain() ),
    'fields' => [
      [
        'id'      => 'react-enable',
        'type'    => 'switch',
        'title'   => __( 'Enable', Theme::domain() ),
        'default' => false,
      ],
    ]
  ]
];