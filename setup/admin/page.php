<?php

use theme\Theme;

$args = [
  'opt_name'              => $this->options_name,
  'display_name'          => sprintf( "%s settings", Theme::name() ),
  'display_version'       => false,
  'page_slug'             => Theme::domain() . '_settings',
  'page_title'            => sprintf( "%s settings", Theme::name() ),
  'intro_text'            => null,
  'footer_text'           => null,
  'menu_type'             => 'menu',
  'menu_title'            => Theme::name(),
  'allow_sub_menu'        => true,
  'page_parent_post_type' => null,
  'customizer'            => true,
  'footer_credit'            => "&nbsp;",
  'dev_mode'            => false,
  'default_show'          => true,
  'show_options_object'   => false,
  'default_mark'          => '*',
  'class'                 => 'theme-settings-container',
  'hints'                 => [
    'icon'          => 'el el-cog',
    'icon_position' => 'left',
    'icon_color'    => '#a0a5aa',
    'icon_size'     => 'normal',
    'tip_style'     => [
      'color'   => 'light',
      'shadow'  => '1',
      'rounded' => '1',
    ],
    'tip_position'  => [
      'my' => 'top left',
      'at' => 'bottom right',
    ],
    'tip_effect'    => [
      'show' => [
        'duration' => '500',
        'event'    => 'mouseover',
      ],
      'hide' => [
        'duration' => '500',
        'event'    => 'mouseleave unfocus',
      ],
    ],
  ],
  'output'                => true,
  'output_tag'            => true,
  'settings_api'          => true,
  'cdn_check_time'        => '1440',
  'page_permissions'      => 'manage_options',
  'save_defaults'         => true,
  'show_import_export'    => true,
  'database'              => 'theme_mod', //theme_mods, options, network
  'transient_time'        => '3600',
  'network_sites'         => true,
  'share_icons'           => [
    [
      'url'   => 'https://gitlab.com/nightar/wp_base',
      'title' => 'Started theme',
      'icon'  => 'el el-github'
    ],
  ]
];