const debug = process.env.NODE_ENV !== 'production';
const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')
const path = require('path');

module.exports = {
  devtool: debug ? 'inline-sourcemap' : false,
  mode: debug ? 'development' : 'production',
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, '../js/'),
    filename: 'react.[name].js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        },
      },
      {
        test: /\.(scss|sass)$/,
        include: [
          path.resolve(__dirname, '/'),
     //     path.resolve(__dirname, 'layouts/')
        ],
        exclude: /node_modules/,
        use: [
          {
            // Adds CSS to the DOM by injecting a `<style>` tag
            loader: 'style-loader'
          },
          {
            // Interprets `@import` and `url()` like `import/require()` and will resolve them
            loader: 'css-loader'
          },
          {
            // Loader for webpack to process CSS with PostCSS
            loader: 'postcss-loader',
            options: {
              plugins: function () {
                return [
                  require('autoprefixer')
                ];
              }
            }
          },
          {
            // Loads a SASS/SCSS file and compiles it to CSS
            loader: 'sass-loader'
          }
        ]
      },
      {
        test: /\.(png|jpg|gif|php)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: "[path][name].[ext]",
            },
          }
        ]
      }
    ]
  },
  externals: {
    jquery: 'jQuery'
  },
  plugins: [
    new BrowserSyncPlugin({
      proxy: "http://192.168.99.100",
      open: 'external',
    }),
  ],
};