import React, { Component } from 'react'
import './style.scss'

class PostArchive extends Component {

  render() {
    const {post} = this.props
    console.log( post )
    return (
      <div className={"card"}>
        <div className={"card-header"}>
          <h2><a href={post.link}>{post.title.rendered}</a></h2>
        </div>
        <div className={"card-body"} dangerouslySetInnerHTML={{__html: post.excerpt.rendered}}>
        </div>
      </div>
    )
  }
}

export default PostArchive