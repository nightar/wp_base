import React, {Component} from 'react'
import PostArchive from '../PostArchive/index'

class PostList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      postsCount: 0,
      postList: []
    };
  }

  componentDidCatch(error, errorInfo) {
    return {hasError: true};
  }

  componentWillMount() {
    fetch(theme_data.home_url + '/wp-json/wp/v2/posts').then(results => (results.json())).then(data => {
        this.setState({postList: data})
        this.setState({postsCount: data.length})
      }
    )
  }

  render() {
    const postElements = this.state.postList.map( post => (<div key={post.id}><PostArchive post={post}/></div>))
    return (
      <div>
      {postElements}
      </div>
    )
  }


}

export default PostList
