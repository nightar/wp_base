<?php

namespace theme;

use theme\Classes\ACF;
use theme\Classes\Actions;
use theme\Admin\Admin;
use theme\Classes\Ajax;
use theme\Classes\Base;
use theme\Classes\CPT;
use theme\Classes\Dependence;
use theme\Classes\GravityForms;
use theme\Classes\Image;
use theme\Classes\Menu;
use theme\Classes\React;
use theme\Classes\Sidebars;
use theme\Classes\Widgets;

/**
 * Main Class Theme
 *
 * @package theme
 */
class Theme extends Base {

  public static $version;

  public static $domain;

  public static $name;

  public static $image;

  public static $cache_expire;

  /**
   * Get theme text domain
   *
   * @return mixed|void
   */
  public static function domain() {

    if ( is_null( self::$domain ) ) {
      $theme = wp_get_theme();

      if ( $theme instanceof \WP_Theme ) {
        self::$domain = $theme->get( 'TextDomain' );
      }
      else {
        self::$domain = 'base';
      }
    }

    return apply_filters( 'theme-domain', self::$domain );
  }

  /**
   * Get theme cache expire seconds
   *
   * @return int
   */
  public static function getCacheExpire() {
    if( is_null( self::$cache_expire ) ) {
      self::$cache_expire = 86400;
    }
    return self::$cache_expire;
  }
  /**
   * Get theme name
   *
   * @return mixed|void
   */
  public static function name() {

    if ( is_null( self::$name ) ) {
      $theme = wp_get_theme();
      if ( $theme instanceof \WP_Theme ) {
        self::$name = $theme->get( 'Name' );
      }
      else {
        self::$name = 'Theme';
      }
    }

    return apply_filters( 'theme-name', self::$name );
  }

  /**
   * Get theme version
   *
   * @return mixed|void
   */
  public static function version() {

    if ( is_null( self::$version ) ) {
      global $wp_version;
      self::$version = $wp_version . '.0.1';
    }

    return apply_filters( 'theme-version', self::$version );
  }

  /**
   * Init theme setups
   */
  public function init() {

    if ( Helper::getThemeDirectory( '/setup/functions.php') ) {
      include_once  Helper::getThemeDirectory( '/setup/functions.php');
    }

    Admin::instance();

    Dependence::instance();

    Sidebars::instance();

    Actions::instance();

    Ajax::instance();

    CPT::instance();

    Menu::instance();

    Widgets::instance();

    React::instance();



    if ( class_exists( '\acf' ) ) {
      ACF::instance();
    }

    if ( class_exists( '\GFForms' ) ) {
      GravityForms::instance();
    }

    self::$image = Image::instance();

    add_action( 'after_setup_theme', [ $this, 'setup', ] );

    add_action( 'themecheck_checks_loaded', [ $this, 'themeTagCheck', ] );

    add_action( 'admin_notices', [ $this, 'seoWarning', ] );

    add_filter( 'the_password_form', [ $this, 'contentPassworForm', ]);

    add_filter( 'admin_init', [ $this, 'optionsCapability', ]);
  }

  public function themeTagCheck() {

    $disabled_checks = [ 'TagCheck', 'Plugin_Territory', 'CustomCheck', 'EditorStyleCheck' ];
    global $themechecks;
    foreach ( $themechecks as $key => $check ) {
      if ( is_object( $check ) && in_array( get_class( $check ), $disabled_checks ) ) {
        unset( $themechecks[ $key ] );
      }
    }
  }

  /**
   * Setup theme
   */
  public function setup() {

    $this->localization();
    $this->supports();
  }

  /**
   * Localization theme
   */
  public function localization() {

    load_theme_textdomain( self::domain(), get_template_directory() . '/languages' );
  }

  /**
   * Setup theme supports
   */
  public function supports() {

    $supports = [ 'automatic-feed-links' => '', 'title-tag' => '', 'html5' => [ 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption', ], 'post-formats' => '', 'post-thumbnails' => '', 'editor-style' => '', ];
    if ( class_exists( '\WooCommerce' ) ) {
      $supports[ 'woocommerce' ] = '';
      $supports[ 'wc-product-gallery-zoom' ] = '';
      $supports[ 'wc-product-gallery-lightbox' ] = '';
      $supports[ 'wc-product-gallery-slider' ] = '';
    }

    $supports = apply_filters( 'theme-supports', $supports );

    if ( ! empty( $supports ) ) {
      foreach ( $supports as $key => $value ) {
        if ( empty( $value ) ) {
          add_theme_support( $key );
        }
        else {
          add_theme_support( $key, $value );
        }
      }
    }
  }

  public function seoWarning() {

    if ( get_option( 'blog_public' ) )
      return;
    $message = __( 'You are blocking access to robots. You must go to your <a href="%s">Reading</a> settings and uncheck the box for Search Engine Visibility.', 'wordlift' );
    echo '<div class="error"><p>';
    printf( $message, admin_url( 'options-reading.php' ) );
    echo '</p></div>';
  }

  public function contentPassworForm() {

    global $post;
    $post = get_post( $post );
    $label = 'pwbox-' . ( empty( $post->ID ) ? rand() : $post->ID );
    $output = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
	<p>' . __( 'This content is password protected. To view it please enter your password below:', Theme::domain() ) . '</p>
	<p><label for="' . $label . '">' . __( 'Password:', Theme::domain() ) . '</label> <input name="post_password" id="' . $label . '" type="password" size="20" /> <input type="submit" name="Submit" value="' . esc_attr__( 'Submit', Theme::domain() ) . '" /></p></form>
	';

    return $output;
  }
  public function optionsCapability() {
    $role = get_role( 'administrator' );
    $role->add_cap( 'theme_options_view' );
  }

}