<?php
/**
 * Created by PhpStorm.
 * User: syallin
 * Date: 26.09.2018
 * Time: 15:19
 */

namespace theme\Widget;


use theme\Helper;
use theme\Theme;

class ACFWidgetBase extends WidgetBase {

  function __construct( $id_base, $name, $widget_options = array(), $control_options = array() ) {

    parent::__construct( $id_base, $name, $widget_options, $control_options );

    $this->intiAcfFields();
  }

  function widget( $args, $instance ) {
    $cache = wp_cache_get( $this->class_name, 'widget' );

    if ( !is_array( $cache ) )
      $cache = array();

    if ( isset( $cache[$args['widget_id']] ) ) {
      echo $cache[$args['widget_id']];
      return;
    }
    $before_widget = '';
    $before_title = '';
    $after_title = '';
    $after_widget = '';
    ob_start();
    extract( $args );

    $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? __( 'Recent Posts', Theme::domain() ) : $instance['title'], $instance, $this->id_base );


    if ( $title ) :
      ?>
      <?php echo $before_widget; ?>
      <?php if ( $title ) echo $before_title . $title . $after_title; ?>
      <?php echo $after_widget; ?>
      <?php
      wp_reset_postdata();

    endif;

    $cache[$args['widget_id']] = ob_get_flush();
    wp_cache_set( $this->class_name, $cache, 'widget' );
  }

  public function fields() {
    return [];
  }

  public  function get_field( $field_name ) {
    $result = '';
    if ( function_exists( 'get_field' ) ) {
      $result = get_field( $field_name, 'widget_' . $this->id );
    }
    return $result;
  }

  public function intiAcfFields() {
    if( function_exists('acf_add_local_field_group') ):

      acf_add_local_field_group(array(
        'key' => 'group_' . $this->class_name,
        'fields' => $this->fields(),
        'location' => array(
          array(
            array(
              'param' => 'widget',
              'operator' => '==',
              'value' => $this->id_base,
            ),
          ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
      ));
    endif;
  }


}