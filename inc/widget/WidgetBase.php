<?php
/**
 * Created by PhpStorm.
 * User: syallin
 * Date: 26.09.2018
 * Time: 15:23
 */

namespace theme\Widget;


use theme\Theme;

class WidgetBase extends \WP_Widget {

  public $class_name;

  function __construct( $id_base, $name, $widget_options = array(), $control_options = array() ) {

    parent::__construct( $id_base ,$name, $widget_options, $control_options );

    add_action( 'switch_theme', array( &$this, 'flush_widget_cache' ) );

  }

  public static function init() {

    \register_widget( self::getClassName() );

  }

  public static function getClassName() {

    return get_called_class();
  }

  function update( $new_instance, $old_instance ) {

    $instance           = $old_instance;
    $instance['title']  = strip_tags( $new_instance['title'] );
    $this->flush_widget_cache();

    $alloptions = wp_cache_get( 'alloptions', 'options' );
    if ( isset( $alloptions[$this->class_name] ) )
      delete_option( $this->class_name );

    return $instance;
  }

  function flush_widget_cache() {
    wp_cache_delete( $this->class_name, 'widget' );
  }

  function form( $instance ) {

    $title	= isset( $instance['title'] )  ? esc_attr( $instance['title'] ) : '';
    ?>
    <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', Theme::domain() ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>
    <?php
  }

}