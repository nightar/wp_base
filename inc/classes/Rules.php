<?php
/**
 * Rules class
 */

namespace theme\Classes;


use theme\Helper;

class Rules extends Base {
  protected $rules = [];

  public function init()
  {
    $rules = [
      'can_comment' => [ self::getClassName(), 'canComments'],
      'react_enable' => [ self::getClassName(), 'isReactEnable'],
    ];
    $this->rules = apply_filters( 'theme-rules', $rules);
  }

  public static function check( $rule ){

    if ( isset( self::instance()->rules[$rule] ) ) {
      try {

        $result = call_user_func( self::instance()->rules[$rule] );
      } catch ( \Exception $e ) {
        $result = false;
      }
    } else {
      try {
        $result = call_user_func( $rule );

      } catch ( \Exception $e ) {
        $result = false;
      }
    }
    return $result;
  }
  public static function canComments() {
    return is_singular() && comments_open() && get_option( 'thread_comments' );
  }

  public static function isReactEnable() {
    return Helper::getOption( 'react-enable' );
  }

}