<?php
/**
 * Acf customization class
 */

namespace theme\Classes;

class ACF extends Base
{

  private $option_pages;

  public function init()
  {
    add_filter('theme-sidebrs', [$this, 'sidebar']);
    add_filter('theme-widgets', [$this, 'widgets']);

    $this->initOptionPages();
    $this->initDynamicSelects();
  }

  /**
   * Add sidebar for acf widgets
   *
   * @param $sidebars
   * @return mixed
   */
  public function sidebar($sidebars)
  {
    /**
     * TO DO
     */
    return $sidebars;

  }

  /**
   * Add widgets classes
   * @param $sidebars
   * @return mixed
   */
  public function widgets($widgets)
  {

    $acf_widgets = [
    ];
    $widgets = array_merge($widgets, $acf_widgets);
    $widgets = array_unique($widgets);
    return $widgets;

  }

  public function initOptionPages()
  {

    $this->option_pages = apply_filters('theme-option-pages', [
      [
        'type' => 'sub_page',
        'title' => 'Theme Options',
        'parent' => 'themes.php',
        'role' => 'theme_options_view',
      ]
    ]);

    if ( ! empty( $this->option_pages ) ) {
      foreach ( $this->option_pages as $page ) {
        $role = isset( $page['role'] ) && ! empty( $page['role'] ) ? current_user_can( $page['role'] ) : true;
        if ( $role ) {
          $type = isset( $page['type'] ) && ! empty( $page['type'] ) ? $page['type'] : 'page';
          unset( $page['role'], $page['type'] );
          switch ( $type ) {
            case 'sub_page' :
              acf_add_options_sub_page( $page );
              break;
            case 'page':
              acf_add_options_page( $page );
              break;
          }
        }
      }
    }
  }
  public function initDynamicSelects() {
    /**
     * TO DO
     */
  }
}