<?php

namespace theme\Classes;


class React extends Base
{
  public function init()
  {
    add_filter( 'template_include', [$this, 'reactTemplate'] );

  }

  /**
   * Return the react template
   */
  public function reactTemplate() {
    return get_template_directory() .'/react.php';
  }

}