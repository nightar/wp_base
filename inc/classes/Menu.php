<?php
/**
 * Class for menu theme
 */

namespace theme\Classes;

use theme\Theme;
use theme\Helper;

class Menu extends Base {

  private $menu_list;

  public function init() {

    $this->initMenu();
    $this->registerMenu();
    add_filter( 'nav_menu_css_class', [$this, 'menuClasses'] );
  }

  public function initMenu() {

    $menu_list = [];
    if ( Helper::getThemeDirectory( '/setup/menu_locations.php') ) {
      include_once  Helper::getThemeDirectory( '/setup/menu_locations.php');
    }
    $this->menu_list = apply_filters( 'theme-menu', $menu_list );

  }


  public function menuClasses( $classes ) {

    $classes = str_replace( [ 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor' ], 'active', $classes );
    $classes = str_replace( [ 'menu-item-has-children' ], 'drop', $classes );
    return $classes;
  }

  public function registerMenu() {

    if ( $this->menu_list ) {
      foreach ( $this->menu_list as $name => $label ) {
        register_nav_menus( [ $name => $label, ] );
      }
    }
  }

}