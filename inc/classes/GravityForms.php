<?php
/**
 * Created by PhpStorm.
 * User: syallin
 * Date: 09.10.2018
 * Time: 16:24
 */

namespace theme\Classes;


use theme\Theme;

class GravityForms extends Base {
  public function init() {
    add_action( 'gform_field_standard_settings', [
      $this,
      'customFieldsSettings',
    ], 10, 2 );
    add_action( 'gform_editor_js', [
      $this,
      'customFieldScript',
    ] );
    add_filter( 'gform_field_choice_markup_pre_render', [$this, 'customChoiceMarkup'], 10, 3);
    add_filter( 'gform_field_css_class', [$this, 'customFieldClass'], 10, 3 );
  }

  public function customFieldsSettings( $position, $form_id ) {
    if( 1368 == $position ) {
      ?>
      <li class="other_choice_label_setting field_setting">
        <label for="field_other_choice_label" class="section_label">
          <?php _e( 'Other Choice Label', Theme::domain() ); ?>
        </label>
        <input type="text" id="field_other_choice_label" class="fieldwidth-3" size="35"
               onchange="SetFieldProperty('otherChoiceLabel', this.value);"/>
      </li>
      <?php

    }
  }

  public function customFieldScript() {
    ?>
    <script type='text/javascript'>
        fieldSettings.text += ', .other_choice_label_setting';
        jQuery(document).on('gform_load_field_settings', function (event, field, form) {
            jQuery('#field_other_choice_label').attr('value', field.otherChoiceLabel);
        });
        gform.addAction('gform_post_load_field_settings', function (data) {
            var field = data[0],
                form = data[1];
            if (field.type == "radio") {
                var enableChoice = jQuery('#field_other_choice'),
                    choiceLabel = jQuery('.other_choice_label_setting');
                var choiceToogle = function () {
                    if (enableChoice.length && enableChoice.is(':checked')) {
                        choiceLabel.show();
                    } else {
                        choiceLabel.hide();
                    }
                };
                choiceToogle();
                enableChoice.on( 'change', choiceToogle );
            }
        })
    </script>
    <?php
  }
  public function customChoiceMarkup( $markup, $input, $field) {
    if( $field instanceof \GF_Field_Radio && isset( $input['value'] ) && $input['value'] == 'gf_other_choice' ) {
      if( $field->otherChoiceLabel ) {
        $markup = str_replace( $input['text'], '', $markup );
        $markup = str_replace('</li>','<span class="placehodler-label">' . $field->otherChoiceLabel  . '</span></li>', $markup);
      }
      $markup = str_replace( "<li class='", "<li class='other-gchoice ", $markup);
    }
    return $markup;
  }
  public function customFieldClass( $classes, $field, $form ) {
    if( $field instanceof  \GF_Field ) {
      if( empty( $field->label ) ) {
        $classes .= ' gf-no-label';
      }
    }
    return $classes;
  }
}