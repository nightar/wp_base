<?php
/**
 * Theme widget class
 */

namespace theme\Classes;

use theme\Helper;

class Widgets extends Base {

  private $widgets;

  public function init() {

    $this->initWidgets();
    $this->registerWidgets();
  }

  public function initWidgets() {
    $widgets = [];

    if ( Helper::getThemeDirectory( '/setup/widgets.php' ) ) {
      include_once Helper::getThemeDirectory( '/setup/widgets.php' );
    }
    $this->widgets = apply_filters( 'theme-widgets', $widgets );
  }

  public function registerWidgets() {

    if ( $this->widgets ) {
      foreach ( $this->widgets as $widget ) {
        if ( class_exists( $widget ) && method_exists( $widget, 'init' ) ) {
          add_action( 'widgets_init', [
            $widget,
            'init',
          ] );
        }
      }
    }
  }

}