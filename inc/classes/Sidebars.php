<?php
namespace theme\Classes;

use theme\Helper;

/**
 * Class Sidebars
 *
 * @package theme\Classes
 */
class Sidebars extends Base {

  public function init() {

    add_action( 'widgets_init', [ $this, 'sidebars', ] );
  }

  /**
   * Register theme sidebars
   */
  public function sidebars() {

    $sidebars = [];
    if ( Helper::getThemeDirectory( '/setup/sidebars.php' ) ) {
      include_once Helper::getThemeDirectory( '/setup/sidebars.php' );
    }
    $sidebars = apply_filters( 'theme-sidebrs', $sidebars );
    foreach ( $sidebars as $sidebar ) {
      if ( is_array( $sidebar ) && isset( $sidebar[ 'id' ] ) ) {
        register_sidebar( $sidebar );
      }
    }
  }

}