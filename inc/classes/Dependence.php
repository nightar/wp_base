<?php
namespace theme\Classes;

use theme\Helper;
use theme\Theme;

/**
 * Class Dependence
 *
 * @package theme\Classes
 */
class Dependence extends Base {

  public function init() {

    add_action( 'wp_enqueue_scripts', [ $this, 'scripts', ] );

    add_action( 'admin_enqueue_scripts', [ $this, 'adminScripts', ] );
  }

  /**
   * Setup theme styles and scripts
   */
  public function scripts() {

    $in_footer = apply_filters( 'scripts-footer', true );
    /*
     * Adds JavaScript to pages with the comment form to support
     * sites with threaded comments (when in use).
     */
    $parametrs = [
      'obj_name'     => 'theme_data',
      'home_url'     => home_url(),
      'ajax_url'     => add_query_arg( [ 'action' => '%%endpoint%%' ], admin_url( 'admin-ajax.php' ) ),
      'scripts_path' => get_template_directory() . '/js/',
      'scripts_uri'  => get_template_directory_uri() . '/js/',
      'scripts'      => [],
    ];
    $parametrs = apply_filters( 'theme-script-parameters', $parametrs );

    $scripts = [];
    $styles = [];

    if ( Helper::getThemeDirectory( '/setup/scripts.php' ) ) {
      include_once Helper::getThemeDirectory( '/setup/scripts.php' );
    }
    if ( Helper::getThemeDirectory( '/setup/styles.php' ) ) {
      include_once Helper::getThemeDirectory( '/setup/styles.php' );
    }

    $scripts = apply_filters( 'theme-scripts', $scripts );
    $styles = apply_filters( 'theme-styles', $styles );

    foreach ( $scripts as $key => $script ) {
      $this->enqueueScript( $key, $script );
    }

    foreach ( $styles as $key => $style ) {
      $this->enqueueStyle( $key, $style );
    }
  }

  /**
   * Setup theme styles and scripts
   */
  public function adminScripts() {

    $scripts = [];
    $styles = [];

    if ( Helper::getThemeDirectory( '/setup/admin_scripts.php' ) ) {
      include_once Helper::getThemeDirectory( '/setup/admin_scripts.php' );
    }
    if ( Helper::getThemeDirectory( '/setup/admin_styles.php' ) ) {
      include_once Helper::getThemeDirectory( '/setup/admin_styles.php' );
    }

    $scripts = apply_filters( 'theme-admin-scripts', $scripts );
    $styles = apply_filters( 'theme-admin-styles', $styles );

    foreach ( $scripts as $key => $script ) {
      $this->enqueueScript( $key, $script );
    }

    foreach ( $styles as $key => $style ) {
      $this->enqueueStyle( $key, $style );
    }
  }

  /**
   * Enqueue script
   *
   * @param string $hook
   * @param array  $data
   */
  protected function enqueueScript( $hook, $data ) {

    $data = array_merge( [
      'src'     => '',
      'deps'    => [],
      'version' => Theme::version(),
      'footer'  => false,
      'rewrite' => false,
      'rules'   => '',
    ], $data );

    if ( $data[ 'rewrite' ] && wp_script_is( $hook, 'registered' ) ) {
      wp_deregister_script( $hook );
    }

    if ( $this->validateEnqueue( $data ) ) {
      wp_enqueue_script( $hook, $data[ 'src' ], $data[ 'deps' ], $data[ 'version' ], $data[ 'footer' ] );
      if ( isset( $data[ 'parameters' ] ) && ! empty( $data[ 'parameters' ] ) ) {
        $name = isset( $data[ 'parameters' ][ 'obj_name' ] ) && ! empty( $data[ 'parameters' ][ 'obj_name' ] ) ? $data[ 'parameters' ][ 'obj_name' ] : $hook . '_data';
        wp_localize_script( $hook, $name, $data[ 'parameters' ] );
      }
    }
  }

  /**
   * Enqueue style
   *
   * @param string $hook
   * @param array  $data
   */
  protected function enqueueStyle( $hook, $data ) {

    $data = array_merge( [
      'src'     => '',
      'deps'    => [],
      'version' => Theme::version(),
      'media'   => 'all',
      'rewrite' => false,
      'rules'   => '',
    ], $data );

    if ( $data[ 'rewrite' ] && wp_style_is( $hook, 'registered' ) ) {
      wp_deregister_style( $hook );
    }

    if ( $this->validateEnqueue( $data ) ) {
      wp_enqueue_style( $hook, $data[ 'src' ], $data[ 'deps' ], $data[ 'version' ], $data[ 'media' ] );
    }
  }

  /**
   * Check valid enqueue script or style
   *
   * @param array $data
   *
   * @return bool|mixed
   */
  protected function validateEnqueue( $data ) {

    $valid = true;
    $exists = Helper::isUrlExists( $data[ 'src' ] );

    if ( ! is_array( $data ) || ! isset( $data[ 'src' ] ) || ! $exists ) {
      $valid = false;
    }
    $callback = isset( $data[ 'rule' ] ) ? $data[ 'rule' ] : '';
    if ( $callback ) {

      $valid = Rules::check( $data[ 'rule' ] );
    }

    return $valid;
  }

}