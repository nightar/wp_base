<?php
/**
 * Theme svg supports
 */
namespace theme\Classes;

use theme\Helper;
use SVG\SVGImage;
use SVG\Writing\SVGWriter;

class SVG {

  private $svg_logo_output;

  public function __construct() {

    $this->svg_logo_output = apply_filters( 'svg-logo-output', true );
    add_filter( 'upload_mimes', [
      $this,
      'support',
    ] );
    add_filter( 'wp_get_attachment_image_src', [
      $this,
      'fixSize',
    ], 99, 3 );
    if ( $this->svg_logo_output ) {
      $this->svgOutup();
    }
  }

  public function support( $file_types ) {

    if ( ! isset( $file_types[ 'svg' ] ) ) {
      $new_filetypes = array();
      $new_filetypes[ 'svg' ] = 'image/svg+xml';
      $file_types = array_merge( $file_types, $new_filetypes );
    }

    return $file_types;
  }

  public function fixSize( $image, $attachment, $size ) {

    $file = get_attached_file( $attachment );
    if ( ! is_file( $file ) ) {
      return $image;
    }
    $type = mime_content_type( $file );
    $path_info = pathinfo( $file );
    if ( $type == 'image/svg+xml' || $type == 'image/svg' || ( $path_info[ 'extension' ] && in_array( $path_info[ 'extension' ], array(
          'svg',
          'svgz',
        ) ) ) ) {

      $image_size = wp_get_additional_image_sizes();

      $svg = SVGImage::fromFile( get_attached_file( $attachment ) );
      $doc = $svg->getDocument();
      $width = $doc->getWidth();
      if ( ! $width ) {
        $width = 1;
      }

      $heigh = $doc->getHeight();
      if ( ! $heigh ) {
        $heigh = 1;
      }

      if ( $size == 'thumbnail' ) {

        $max_width = get_option( 'thumbnail_size_w' );
        $max_height = get_option( 'thumbnail_size_h' );

      }
      elseif ( $size == 'large' ) {

        $max_width = get_option( 'large_size_w' );
        $max_height = get_option( 'large_size_h' );

      }
      elseif ( $size == 'medium' ) {

        $max_width = get_option( 'medium_size_w' );
        $max_height = get_option( 'medium_size_h' );

      }
      elseif ( isset( $image_size[ $size ] ) ) {

        $max_width = $image_size[ $size ][ 'width' ];
        $max_height = $image_size[ $size ][ 'height' ];

      }
      else {

        $max_width = $width;
        $max_height = $heigh;

      }

      $dims = \image_resize_dimensions( $width, $heigh, $max_width, $max_height, false );

      if ( $dims ) {

        list( $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h ) = $dims;
        $image[ 1 ] = $dst_w;
        $image[ 2 ] = $dst_h;

      }
      elseif ( $width && $heigh ) {

        $image[ 1 ] = $width;
        $image[ 2 ] = $heigh;

      }
    }

    return $image;
  }

  /**
   * Set output image as svg tag
   */
  public function svgOutup() {

    add_filter( 'get_custom_logo', [
      $this,
      'svgLogo',
    ] );

  }

  public function svgLogo( $html ) {

    $custom_logo_id = get_theme_mod( 'custom_logo' );
    if ( $custom_logo_id ) {
      $mime = get_post_mime_type( $custom_logo_id );
      if ( $mime == 'image/svg' || $mime == 'image/svg+xml' || $mime == 'image/svg+xmlx' ) {
        $svg_html = $this::getImage( $custom_logo_id );
        if ( $svg_html ) {
          $html = preg_replace( "|(\<img[^\>]*\/?\>)|si", $svg_html, $html );
        }
      }
    }

    return $html;
  }

  /**
   * Get image with svg tag support
   *
   * @param        $image_id
   * @param string $size
   * @param bool   $icon
   * @param string $attr
   * @return string
   */
  public static function getImage( $image_id, $size = 'full', $icon = false, $attr = '' ) {

    $html = '';
    $mime = get_post_mime_type( $image_id );

    if ( $mime == 'image/svg' || $mime == 'image/svg+xml' || $mime == 'image/svg+xmlx' ) {
      $svg = SVGImage::fromFile( get_attached_file( $image_id ) );
      $doc = $svg->getDocument();
      $docWidth = $doc->getWidth();
      $docHeight = $doc->getHeight();
      $viewBox = $doc->getViewBox();
      if ( ( ! $docWidth || ! $docHeight ) && $viewBox ) {
        $docWidth = $docWidth ? $docWidth : $viewBox[ 2 ];
        $docHeight = $docHeight ? $docHeight : $viewBox[ 3 ];
      }
      $ratio = [];
      if ( $size && $size !== 'full' ) {
        $sizeWidth = Helper::getSizeWidth( $size );
        $sizeHeight = Helper::getSizeHeight( $size );
        $ratio = wp_constrain_dimensions( ( $docWidth * 10000 ), ( $docHeight * 10000 ), $sizeWidth, $sizeHeight );
      }

      if ( ! empty( $ratio ) ) {
        $doc->setWidth( $ratio[ 0 ] );
        $doc->setHeight( $ratio[ 1 ] );
      }
      else {
        $doc->setWidth( $docWidth );
        $doc->setHeight( $docHeight );
      }
      $ratio = $doc->getAttribute( 'preserveAspectRatio' );
      if ( ! $ratio ) {
        $doc->setAttribute( 'preserveAspectRatio', 'xMidYMid slice' );
      }
      if ( is_array( $attr ) && ! empty( $attr ) ) {
        $attr = apply_filters( 'wp_get_attachment_image_attributes', $attr, get_post( $image_id ), $size );
        $attr = array_map( 'esc_attr', $attr );
        foreach ( $attr as $key => $value ) {
          if ( is_object( $value ) ) {
            $value = base64_encode( serialize( $value ) );
          }
          elseif ( is_array( $value ) ) {
            $value = implode( ' ', $value );
          }
          $key = is_string( $key ) ? $key : sanitize_title( $value );
          if ( $key && $value ) {
            $doc->setAttribute( $key, $value );
          }
        }
      }

      $writer = new SVGWriter( true );
      $writer->writeNode( $doc );
      $html = $writer->getString();
    }
    else {
      $html = wp_get_attachment_image( $image_id, $size, $icon, $attr );
    }

    return $html;
  }

}