<?php
/**
 * Ajax theme class
 */
namespace theme\Classes;


use theme\Theme;

class Ajax extends Base {
  public function init() {
    add_action( 'wp_ajax_ajax-load-more', [
      $this,
      'loadMore',
    ] );
    add_action( 'wp_ajax_nopriv_ajax-load-more', [
      $this,
      'loadMore',
    ] );

  }

  public function loadMore() {
    if( isset( $_REQUEST[ 'query' ] ) && ! empty( $_REQUEST[ 'query' ] ) ) {
      $data            = $_REQUEST[ 'query' ];
      $data            = base64_decode( $data );
      $data            = json_decode( $data, true );
      $page            = isset( $data[ 'paged' ] ) && ! empty( $data[ 'paged' ] ) ? (int) $data[ 'paged' ] : 1;
      $data[ 'paged' ] = $page + 1;

      $query_list = new \WP_Query( $data );

      if( $query_list->have_posts() ) {
        $query_data = json_encode( $data );
        $query_data = base64_encode( $query_data );
        while( $query_list->have_posts() ) {
          $query_list->the_post();
          get_template_part( 'blocks/archive/content', get_post_type() );
        }
        if( $query_list->max_num_pages > $page ) {
          echo '<a href="' . add_query_arg( [
              'action' => 'ajax-load-more',
              'query'  => $query_data,
            ], admin_url( 'admin-ajax.php' ) ) . '" class="load-more">' . __( 'Load More', Theme::domain() ) . '</a>';
        }
      }
      wp_reset_postdata();
    }
    exit;
  }

  public function getInput( $name, $prefix = 'filter-' ) {
    static $data = [];
    $result = '';
    if( isset( $data[ $name ] ) && ! empty( $data[ $name ] ) ) {
      $result = $data[ $name ];
    }
    elseif( isset( $data[ $prefix . $name ] ) && ! empty( $data[ $prefix . $name ] ) ) {
      $result = $data[ $prefix . $name ];
    }
    else {

      if( isset( $_REQUEST[ $name ] ) ) {
        $result = esc_attr( $_REQUEST[ $name ] );
      }
      elseif( isset( $_REQUEST[ $prefix . $name ] ) ) {
        $result = esc_attr( $_REQUEST[ $prefix . $name ] );
      }
      $data[$name] = $result;
      $data[$prefix . $name] = $result;
    }
    return apply_filters( 'theme-input-value', $result, $_REQUEST);
  }

}