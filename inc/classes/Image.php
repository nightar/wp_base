<?php

namespace theme\Classes;

use theme\Helper;

/**
 * Class for manage image Image
 *
 * @package melbourneclub\Classes
 */
class Image extends Base {

  public $thumbnails;

  public static $SVG;

  public function init() {

    $this->thumbnails();

    $this->thumbnails = wp_get_additional_image_sizes();

    self::$SVG = new SVG();

  }

  /**
   * Register theme thumbanils
   */
  public function thumbnails() {

    $thumbanils = [];

    if ( Helper::getThemeDirectory( '/setup/image_sizes.php' ) ) {
      include_once Helper::getThemeDirectory( '/setup/image_sizes.php' );
    }

    $thumbanils = apply_filters( 'theme-thumbnails', $thumbanils );
    foreach ( $thumbanils as $thumbanil ) {
      if ( is_array( $thumbanil ) && isset( $thumbanil[ 'name' ] ) ) {
        $thumbanil = array_merge( [
          'name'   => '',
          'width'  => 0,
          'height' => 0,
          'crop'   => false,
        ], $thumbanil );
        if ( $thumbanil[ 'width' ] && $thumbanil[ 'height' ] ) {
          add_image_size( $thumbanil[ 'name' ], $thumbanil[ 'width' ], $thumbanil[ 'height' ], $thumbanil[ 'crop' ] );
        }
      }
    }
  }

  public static function getID( $image ) {

    $id = null;
    if ( is_array( $image ) && isset( $image[ 'id' ] ) ) {
      $id = intval( $image[ 'id' ] );
    }
    elseif ( is_numeric( $image ) ) {
      $id = intval( $image );
    }
    elseif ( is_string( $image ) ) {
      $key = sanitize_key( $image ) . '-get-id';
      if ( ( $id = wp_cache_get( $key ) ) === false ) {
        $fix_url = preg_replace( "|(.*)(-[\d]*x[\d]*)(\..*)$|", "$1$3", $image );
        global $wpdb;
        $ids = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE (guid='%s') OR (guid='%s')", $image, $fix_url ) );
        if ( $ids ) {
          $id = (int) $ids[ 0 ];
        }
        wp_cache_set( $key, $id );
      }
    }

    return $id;
  }

  /**
   * Get size information for all currently-registered image sizes.
   *
   * @global $_wp_additional_image_sizes
   * @uses   get_intermediate_image_sizes()
   * @return array $sizes Data for all currently-registered image sizes.
   */
  public static function getImageSizes() {

    global $_wp_additional_image_sizes;

    $sizes = [];

    foreach ( get_intermediate_image_sizes() as $_size ) {
      if ( in_array( $_size, [ 'thumbnail', 'medium', 'medium_large', 'large', ] ) ) {
        $sizes[ $_size ][ 'width' ] = get_option( "{$_size}_size_w" );
        $sizes[ $_size ][ 'height' ] = get_option( "{$_size}_size_h" );
        $sizes[ $_size ][ 'crop' ] = (bool) get_option( "{$_size}_crop" );
      }
      elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
        $sizes[ $_size ] = [
          'width'  => $_wp_additional_image_sizes[ $_size ][ 'width' ],
          'height' => $_wp_additional_image_sizes[ $_size ][ 'height' ],
          'crop'   => $_wp_additional_image_sizes[ $_size ][ 'crop' ],
        ];
      }
    }

    return $sizes;
  }

  /**
   * Get size information for a specific image size.
   *
   * @uses   get_image_sizes()
   *
   * @param  string $size The image size for which to retrieve data.
   *
   * @return bool|array $size Size data about an image size or false if the size doesn't exist.
   */
  public static function getSizeInfo( $size ) {

    $sizes = self::getImageSizes();

    if ( isset( $sizes[ $size ] ) ) {
      return $sizes[ $size ];
    }

    return false;
  }

  /**
   * Get the width of a specific image size.
   *
   * @uses   get_image_size()
   *
   * @param  string $size The image size for which to retrieve data.
   *
   * @return bool|string $size Width of an image size or false if the size doesn't exist.
   */
  public static function getSizeWidth( $size ) {

    if ( ! $size = self::getSizeInfo( $size ) ) {
      return false;
    }

    if ( isset( $size[ 'width' ] ) ) {
      return $size[ 'width' ];
    }

    return false;
  }

  /**
   * Get the height of a specific image size.
   *
   * @uses   get_image_size()
   *
   * @param  string $size The image size for which to retrieve data.
   *
   * @return bool|string $size Height of an image size or false if the size doesn't exist.
   */
  public static function getSizeHeight( $size ) {

    if ( ! $size = self::getSizeInfo( $size ) ) {
      return false;
    }

    if ( isset( $size[ 'height' ] ) ) {
      return $size[ 'height' ];
    }

    return false;
  }

}