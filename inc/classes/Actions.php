<?php
/**
 * Theme actions and filters
 */

namespace theme\Classes;

use theme\Helper;

class Actions extends Base {

  public function init() {

    add_action( 'all', [
      $this,
      'getTemplate',
    ] );
    add_filter( 'body_class', [
      $this,
      'bodyClass',
    ] );

    add_filter( 'get_the_archive_title', [
      $this,
      'getArchiveTitle',
    ] );

  }

  public function getTemplate( $hook ) {

    $args = func_get_args();
    if ( strpos( $hook, 'location-' ) === 0 ) {
      $name = isset( $args[ 1 ] ) ? $args[ 1 ] : Helper::pageType();
      $slug = substr( $hook, 9 );
      if ( ! apply_filters( 'custom-theme-location', false, $slug, $name ) ) {
        get_template_part( $slug, $name );
      }

    }

  }

  public function bodyClass( $classes ) {

    /**
     * TO DO
     */
    return $classes;
  }

  public function getArchiveTitle( $title ) {

    if ( is_home() && ( $blog_id = get_option( 'page_for_posts' ) ) ) {
      $title = get_the_title( $blog_id );
    }

    return $title;
  }

}