<?php
/**
 * Base theme class
 *
 * Abstract base class for inheritance
 *
 * @package      theme
 * @subpackage   Classes
 * @category     theme
 */
namespace theme\Classes;

use Doctrine\Common\Annotations\Annotation\Required;

/**
 * Abstract base class for inheritance
 *
 * @since       1.0.0
 * @package theme\Classes
 */
abstract class Base {

  /**
   * Container for base classes instances
   *
   * @access private
   * @var array
   */
  private static $_instance;

  /**
   * Get instance of current class
   *
   * @return class
   */
  public static function instance() {

    $class = get_called_class();

    if( ! isset( self::$_instance[ $class ] ) || empty( self::$_instance[ $class ] ) ) {

      self::$_instance[ $class ] = new static();
    }

    return self::$_instance[ $class ];
  }

  /**
   * Base constructor.
   */
  protected function __construct() {
    $this->init();
  }

  /**
   * Required class
   *
   * @Required
   * @return mixed
   */
  abstract function init();

  /**
   * Get current class name
   *
   * @return string
   */
  public static function getClassName() {

    return get_called_class();
  }

}