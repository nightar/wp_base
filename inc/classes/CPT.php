<?php
/**
 * Theme CPT
 */

namespace theme\Classes;

use theme\Theme;
use PostTypes\PostType;
use PostTypes\Taxonomy;

class CPT extends Base {

  public function init() {
    /**
     * TODO something
     */
  }

  /**
   * Get top term
   *
   * @param int|\WP_Term $term
   * @return array|\WP_Error|\WP_Term|null
   */
  static public function getTopTerm( $term ) {

    if( ( $term = get_term( $term ) ) && ( $term->parent == 0 ) ) {
      return $term;
    }
    else {
      return self::getTopTerm( $term->parent );
    }
  }

  /**
   * Get limited post excerpt
   *
   * @param int    $charlength
   * @param string $end
   * @param null   $id
   * @return mixed|void
   */
  public static function getExcerptLimit( $charlength = 360, $end = '...', $id = null ) {

    $excerpt = get_the_excerpt( $id );
    $charlength++;

    $end_count = strlen( $end );
    if( strlen( $excerpt ) > $charlength ) {
      $subex   = substr( $excerpt, 0, $charlength - $end_count );
      $exwords = explode( ' ', $subex );
      $excut   = -( strlen( $exwords[ count( $exwords ) - 1 ] ) );
      if( $excut < 0 ) {
        $result = substr( $subex, 0, $excut );
      }
      else {
        $result = $subex;
      }
      $result .= $end;
    }
    else {
      $result = $excerpt;
    }

    return apply_filters( 'the_excerpt', $result );
  }

}