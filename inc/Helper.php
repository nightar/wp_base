<?php
/**
 * Created by PhpStorm.
 * User: Snight
 * Date: 25.09.2018
 * Time: 21:24
 */

namespace theme;

use theme\Admin\Admin;

/**
 * Class Helper
 *
 * Helps theme functions
 *
 * @package theme
 */
class Helper {

  protected static $currentPageType;

  private static $options;

  /**
   * Get page template type
   *
   * @return string
   */
  public static function pageType() {

    if( is_null( self::$currentPageType ) ) {
      $obj = get_queried_object();
      if( function_exists( 'is_woocommerce' ) && is_woocommerce() ) {
        if( function_exists( 'is_product' ) && is_product() ) {
          self::$currentPageType = $obj->post_type;
        }
        elseif( function_exists( 'is_shop' ) && is_shop() ) {
          self::$currentPageType = 'wc_shop';
        }
        elseif( is_archive() ) {
          $display_type          = function_exists( 'woocommerce_get_loop_display_mode' ) ? woocommerce_get_loop_display_mode() : '';
          self::$currentPageType = $display_type ? 'wc_archive_' . $display_type : 'wc_archive';
        }
      }
      elseif( is_front_page() ) {
        self::$currentPageType = 'front';
      }
      elseif( is_home() ) {
        self::$currentPageType = 'blog';
      }
      elseif( $obj && $obj instanceof \WP_Post ) {
        self::$currentPageType = $obj->post_type;
      }
      elseif( $obj && $obj instanceof \WP_Term ) {
        self::$currentPageType = $obj->taxonomy;
      }
      elseif( $obj && $obj instanceof \WP_Post_Type ) {
        self::$currentPageType = $obj->name;
      }
      elseif( $obj && $obj instanceof \WP_User ) {
        self::$currentPageType = 'author';
      }
      elseif( is_search() ) {
        self::$currentPageType = 'search';
      }
      elseif( is_date() || is_month() || is_day() || is_year() ) {
        self::$currentPageType = 'date';
      }
      elseif( is_404() ) {
        self::$currentPageType = '404';
      }
      else {
        self::$currentPageType = '';
      }
    }

    return self::$currentPageType;
  }

  /**
   * Get current page title
   *
   * @return string|void
   */
  public static function pageTitle() {

    $title = '';
    if( is_singular() ) {
      $title = get_the_title();
    }
    elseif( is_home() && ! is_front_page() && ( $blog_id = get_option( 'page_for_posts' ) ) ) {
      $title = get_the_title( $blog_id );
    }
    elseif( is_search() ) {
      $title = __( 'Search', Theme::domain() );
    }
    elseif( is_archive() ) {

      $title = get_the_archive_title();
    }
    elseif( is_404() ) {
      $title = __( '404 Page', Theme::domain() );
    }

    return $title;
  }

  /**
   * Get html link
   *
   * @param array $data
   * @param array $args
   * @return string
   */
  public static function link( $data, $args = [] ) {

    if( ! is_array( $data ) ) {
      return '';
    }
    $data = array_merge( [
      'before_title' => '',
      'title'        => '',
      'after_title'  => '',
      'url'          => '',
      'target'       => '',
      'rel'          => '',
      'follow'       => '',
    ], $data );
    $html = '';

    if( ! isset( $args[ 'target' ] ) && ! empty( $data[ 'target' ] ) ) {
      $args[ 'target' ] = $data[ 'target' ];
    }
    if( ! isset( $args[ 'follow' ] ) && ! empty( $data[ 'follow' ] ) ) {
      $args[ 'follow' ] = $data[ 'follow' ];
    }
    if( ! isset( $args[ 'rel' ] ) && ! empty( $data[ 'rel' ] ) ) {
      $args[ 'rel' ] = $data[ 'rel' ];
    }
    $atts = self::getAttrString( $args );

    if( ! empty( $data[ 'title' ] ) && ! empty( $data[ 'url' ] ) ) {
      $html = sprintf( '<a href="%1$s"%2$s>%3$s%4$s%5$s</a>', esc_url( $data[ 'url' ] ), $atts, $data[ 'before_title' ], $data[ 'title' ], $data[ 'after_title' ] );
    }

    return $html;
  }

  /**
   * Conver array to attributes string
   *
   * @param array $atts
   * @return string
   */
  public static function getAttrString( array $atts ) {

    $string = '';
    $atts   = array_filter( $atts );
    if( ! empty( $atts ) ) {
      foreach( $atts as $key => $value ) {
        if( is_object( $value ) ) {
          $value = base64_encode( serialize( $value ) );
        }
        elseif( is_array( $value ) ) {
          $value = array_filter( $value );
          $value = implode( ' ', $value );
        }
        $key = is_string( $key ) ? $key : sanitize_title( $value );
        if( $key && $value ) {
          $string .= ' ' . $key . '="' . $value . '"';
        }
      }
    }

    return $string;
  }

  /**
   * Check url is exists
   *
   * @param $url
   * @return bool
   */
  public static function isUrlExists( $url ) {

    $theme_url = get_stylesheet_directory_uri();

    if( strpos( $url, $theme_url ) === 0 ) {
      $theme_dir = get_stylesheet_directory();
      $url       = str_replace( $theme_url, $theme_dir, $url );
      $status    = file_exists( $url );
    }
    elseif( function_exists( 'curl_version' ) ) {
      $ch = curl_init( $url );
      curl_setopt( $ch, CURLOPT_NOBODY, true );
      curl_exec( $ch );
      $code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

      if( $code == 200 ) {
        $status = true;
      }
      else {
        $status = false;
      }
      curl_close( $ch );
    }
    else {
      $header = wp_safe_remote_head( $url );
      if( isset( $header[ 'response' ][ 'code' ] ) && $header[ 'response' ][ 'code' ] == 200 ) {
        $status = true;
      }
      else {
        $status = false;
      }
    }

    return $status;
  }

  /**
   * Theme buffer
   *
   * @param            $name
   * @param mixed|null $value
   * @param bool       $reset
   * @return mixed|null
   */
  public static function buffer( $name, $value = null, $reset = false ) {

    static $buffer = [];
    if( ! is_null( $value ) ) {
      $buffer[ $name ] = $value;
    }
    $result = isset( $buffer[ $name ] ) ? $buffer[ $name ] : null;
    if( $reset ) {
      unset( $buffer[ $name ] );
    }

    return $result;
  }

  /**
   * Get theme logo
   *
   * @param null $types
   * @param int  $blog_id
   * @return mixed|void|null
   */
  public static function getLogo( $types = null, $blog_id = 0 ) {

    if( empty( $types ) ) {
      $types = [
        'primary',
        'secondary',
      ];
    }
    elseif( ! is_array( $types ) ) {
      $types = [ $types ];
    }

    $html          = '';
    $switched_blog = false;

    $logotypes = [];

    if( is_multisite() && ! empty( $blog_id ) && (int) $blog_id !== get_current_blog_id() ) {
      switch_to_blog( $blog_id );
      $switched_blog = true;
    }
    $html         = [];
    $primary_logo = '';
    if( $types ) {
      foreach( $types as $type ) {
        $value = self::getOption( $type . '-logo' );
        if( $type == 'primary' ) {
          $primary_logo = $value;
        }
        elseif( ( empty( $value ) || ! $value[ 'id' ] ) && $primary_logo ) {
          $value = $primary_logo;
        }
        $logotypes[ $type ] = is_array( $value ) ? $value : [ 'id' => (int) $value ];
        $logo_attr          = [
          'class'    => "custom-logo {$type}-logo",
          'itemprop' => 'logo',
        ];
        $logo_attr[ 'alt' ] = get_post_meta( $logotypes[ $type ][ 'id' ], '_wp_attachment_image_alt', true );
        if( empty( $logo_attr[ 'alt' ] ) ) {
          $logo_attr[ 'alt' ] = get_bloginfo( 'name', 'display' );
        }
        if( isset( $logotypes[ $type ][ 'id' ] ) && ! empty( $logotypes[ $type ][ 'id' ] ) ) {
          $html[] = wp_get_attachment_image( $logotypes[ $type ][ 'id' ], 'full', false, $logo_attr );
        }
        elseif( is_customize_preview() ) {
          $html[] = "<img class=\"custom-logo {$type}-logo\"/>";
        }
      }
    }

    if( empty( $html ) ) {
      return null;
    }


    $html_string = sprintf( '<a href="%1$s" class="logo-link" rel="home" itemprop="url">%2$s</a>', esc_url( home_url( '/' ) ), implode( "", $html ) );


    if( $switched_blog ) {
      restore_current_blog();
    }

    return apply_filters( 'get_logo', $html_string, $blog_id );
  }

  /**
   * Get theme options from Redux or Worpdress data
   *
   * @param string $id field id
   */
  public static function getOption( $id, $format = false ) {

    if( ! isset( self::$options ) ) {
      $redux = get_option( Admin::getOptionsName() );
      $allOptions = wp_load_alloptions();
      self::$options = array_merge( $allOptions, $redux);
      wp_cache_add( 'alloptions', self::$options, 'options', Theme::getCacheExpire() );
    }
    if( isset( self::$options[ $id ] ) ) {
      $value = self::$options[ $id ];
    }
    else {
     $value = false;
    }
    if( $format ) {
      if( is_array( $value ) && is_string( $format ) && isset( $value[ $format ] ) ) {
        $value = $value[ $format ];
      }
      elseif( is_numeric( $value ) && is_attachment( $value ) ) {
        switch( $format ) {
          case 'url':
            $value = wp_get_attachment_url( $value );
          case 'object':
            $value = get_post( $value );
            break;
        }
      }
    }

    return $value;
  }

  /**
   * Get vide details for Youtube or Vimeo services
   *
   * @param string $url
   * @return array|bool|mixed|object
   */
  public static function getVideoDetails( $url ) {

    if( $cache_data = wp_cache_get( sanitize_key( $url ) ) ) {
      return $cache_data;
    } else {
      $host = explode( '.', str_replace( 'www.', '', strtolower( parse_url( $url, PHP_URL_HOST ) ) ) );
      $host = isset( $host[ 0 ] ) ? $host[ 0 ] : $host;
      $data = [];
      switch( $host ) {
        case 'vimeo':
          $pattern = '%(https?:\/\/)?(www\.)?(player\.)?vimeo\.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*%i';
          if( preg_match( $pattern, $url, $match ) ) {
            $id       = $match[ 5 ];
            $test_url = 'https://vimeo.com/' . $id;
            $provider = 'https://vimeo.com/api/oembed.json';

            $provider = add_query_arg( 'url', urlencode( $test_url ), $provider );
            $provider = add_query_arg( 'dnt', 1, $provider );
            $response = wp_safe_remote_request( $provider );
            if( 501 == wp_remote_retrieve_response_code( $response ) ) {
              return new WP_Error( 'not-implemented' );
            }
            if( ! $body = wp_remote_retrieve_body( $response ) ) {
              $data = [
                'provider_name' => 'Vimeo',
                'provider_url'  => 'https://vimeo.com/',
                'id'            => $id,
                'video_url'     => $test_url,
                'embed_url'     => 'https://player.vimeo.com/video/' . $id,
              ];
            }
            else {
              $data = json_decode( $body, true );
              if( ! isset( $data[ 'id' ] ) || empty( $data[ 'id' ] ) ) {
                $data[ 'id' ] = $id;
              }
              $data[ 'video_url' ] = $test_url;
              $data[ 'embed_url' ] = 'https://player.vimeo.com/video/' . $id;
            }
          }
          break;
        case 'youtube':
          $pattern = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i';
          if( preg_match( $pattern, $url, $match ) ) {
            $id       = $match[ 1 ];
            $test_url = 'https://www.youtube.com/watch?v=' . $id;
            $provider = 'https://www.youtube.com/oembed';
            $provider = add_query_arg( 'url', urlencode( $test_url ), $provider );
            $provider = add_query_arg( 'dnt', 1, $provider );
            $provider = add_query_arg( 'format', 'json', $provider );
            $response = wp_safe_remote_request( $provider );
            if( 501 == wp_remote_retrieve_response_code( $response ) ) {
              return new WP_Error( 'not-implemented' );
            }
            if( ! $body = wp_remote_retrieve_body( $response ) ) {
              $data = [
                'provider_name' => 'YouTube',
                'provider_url'  => 'https://www.youtube.com/',
                'id'            => $id,
                'video_url'     => $test_url,
                'embed_url'     => 'https://www.youtube.com/embed/' . $id,
              ];
            }
            else {
              $data = json_decode( $body, true );
              if( ! isset( $data[ 'id' ] ) || empty( $data[ 'id' ] ) ) {
                $data[ 'id' ] = $id;
              }
              $data[ 'video_url' ] = $test_url;
              $data[ 'embed_url' ] = 'https://www.youtube.com/embed/' . $id;
            }
          }
          break;
      }
      wp_cache_set( sanitize_key( $url), $data, '', Theme::getCacheExpire() );
    }
    return empty( $data ) ? false : $data;
  }

  /**
   * Minfy html string
   *
   * @param string $buffer
   * @return mixed|string|string[]|null
   */
  static public function minifyHtml( $buffer ) {

    $search = [
      '/\>[^\S ]+/s',
      // strip whitespaces after tags, except space
      '/[^\S ]+\</s',
      // strip whitespaces before tags, except space
      '/(\s)+/s',
      // shorten multiple whitespace sequences
      '/<!--(.|\s)*?-->/'
      // Remove HTML comments
    ];

    $replace = [
      '>',
      '<',
      '\\1',
      '',
    ];

    $buffer = preg_replace( $search, $replace, $buffer );
    $buffer = str_replace( [
      "\n",
      "\r",
      "\t",
    ], '', $buffer );

    return $buffer;

  }

  /**
   * Get theme directory
   *
   * @param string $path
   * @return string
   */
  static function getThemeDirectory( $path ) {

    if( is_child_theme() && ( is_dir( get_stylesheet_directory() . $path ) || file_exists( get_stylesheet_directory() . $path ) ) ) {
      return get_stylesheet_directory() . $path;
    }
    elseif( is_dir( get_template_directory() . $path ) || file_exists( get_template_directory() . $path ) ) {
      return get_template_directory() . $path;
    }
    else {
      return '';
    }
  }

  /**
   * Get theme directory uri
   *
   * @param string $path
   * @return string
   */
  static function getThemeDirectoryURI( $path ) {

    if( is_child_theme() && ( is_dir( get_stylesheet_directory() . $path ) || file_exists( get_stylesheet_directory() . $path ) ) ) {
      return get_stylesheet_directory_uri() . $path;
    }
    elseif( is_dir( get_template_directory() . $path ) || file_exists( get_template_directory() . $path ) ) {
      return get_template_directory_uri() . $path;
    }
    else {
      return is_child_theme() ? get_stylesheet_directory_uri() . $path : get_template_directory_uri() . $path;
    }
  }

  /**
   * Takes the color hex value and converts to a rgba.
   *
   * @param string $hex
   * @param string $alpha
   * @return string
   */
  public static function hex2rgba( $hex, $alpha = '' ) {
    $hex = str_replace( "#", "", $hex );
    if( strlen( $hex ) == 3 ) {
      $r = hexdec( substr( $hex, 0, 1 ) . substr( $hex, 0, 1 ) );
      $g = hexdec( substr( $hex, 1, 1 ) . substr( $hex, 1, 1 ) );
      $b = hexdec( substr( $hex, 2, 1 ) . substr( $hex, 2, 1 ) );
    }
    elseif( strlen( $hex ) > 5 ) {
      $r = hexdec( substr( $hex, 0, 2 ) );
      $g = hexdec( substr( $hex, 2, 2 ) );
      $b = hexdec( substr( $hex, 4, 2 ) );
    }
    $rgb = $r . ',' . $g . ',' . $b;

    if( '' == $alpha ) {
      return $rgb;
    }
    else {
      $alpha = floatval( $alpha );

      return 'rgba(' . $rgb . ',' . $alpha . ')';
    }
  }

}