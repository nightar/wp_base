<?php
namespace theme;
/**
 * Class Autoloader
 */
class Autoloader {

  /**
   * Autoloader instance
   *
   * @var Autoloader
   */
  public static $loader;

  /**
   * Loader dir base
   *
   * @var string
   */
  public $loaderDir;

  protected $stack;

  /**
   * Lader base namespace
   *
   * @var mixed|void
   */
  public $namespacePrefix;

  /**
   * Loader namespace length
   *
   * @var int
   */
  public $namespaceLength;

  /**
   * Get autoloder instance
   *
   * @return Autoloader
   */
  public static function register() {

    if ( self::$loader == null ) {
      self::$loader = new Autoloader();
    }

    return self::$loader;
  }

  /**
   * Autoloader constructor.
   *
   * @throws Exception
   */
  public function __construct() {

    $autoloader = get_template_directory() . '/vendor/autoload.php';

    if ( is_file( $autoloader ) ) {
      require_once $autoloader;
    }

    $this->loaderDir = __DIR__ . DIRECTORY_SEPARATOR;
    $theme = wp_get_theme();

    if ( $theme instanceof \WP_Theme ) {
      $this->namespacePrefix = apply_filters( 'autoloader-namespace', 'theme' );
      $this->namespaceLength = strlen( $this->namespacePrefix ) + 1;
    }

    spl_autoload_register( array(
      $this,
      'load',
    ) );
  }

  /**
   * Load class file if is possible
   *
   * @param string $class_name
   */
  public function load( $class_name ) {

    $real_class = $class_name;
    $class_name = trim( $class_name, '\\' );
    if ( stripos( $class_name, $this->namespacePrefix ) === 0 && $this->namespaceLength ) {
      $class_name = substr( $class_name, $this->namespaceLength );
    }

    if ( $class_name ) {

      $class_name = $this->fixClassPath( $class_name );
      $class_path = $this->loaderDir . $class_name . '.php';
      if ( is_file( $class_path ) ) {
        include_once $class_path;
      }
    }
  }

  public function fixClassPath( $path ) {

    $path = str_replace( [
      '\\',
      '/',
    ], DIRECTORY_SEPARATOR, $path );
    if ( file_exists( $this->loaderDir . $path . '.php' ) ) {
      return $path;
    }
    $parts = explode( DIRECTORY_SEPARATOR, $path );
    $length = count( $parts ) - 1;
    $new_path = [];
    foreach ( $parts as $key => $folder ) {
      $variations = [
        $folder,
        ucfirst( $folder ),
        strtolower( $folder ),
        strtoupper( $folder ),
      ];
      $variations = array_unique( $variations );
      $dir_exist = false;
      foreach ( $variations as $var ) {
        $new_path[ $key ] = $var;
        $test = $this->loaderDir . implode( DIRECTORY_SEPARATOR, $new_path );

        if ( $key < $length ) {
          if ( is_dir( $test ) ) {
            $dir_exist = true;
            break;
          }
        }
        else {
          $test .= '.php';
          if ( is_file( $test ) ) {
            $dir_exist = true;
            break;
          }
        }
      }
      if ( ! $dir_exist ) {
        return $path;
      }
    }

    return ! empty( $new_path ) ? implode( DIRECTORY_SEPARATOR, $new_path ) : $path;

  }
}

/**
 * Init autoloader
 */
Autoloader::register();