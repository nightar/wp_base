<?php
if ( ! function_exists( 'redux_disable_dev_mode' ) ) {
  function redux_disable_dev_mode( $redux ) {
    if ( $redux->args['opt_name'] != 'redux_demo' ) {
      $redux->args['dev_mode'] = false;
      $redux->args['forced_dev_mode_off'] = false;
    }
  }
}
add_action( 'redux/construct', 'redux_disable_dev_mode' );
