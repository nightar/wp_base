<?php

use theme\Theme;

$domain = Theme::domain();

// All extensions placed within the extensions directory will be auto-loaded for your Redux instance.
Redux::setExtensions( $domain, dirname( __FILE__ ) . '/extensions/' );

// Any custom extension configs should be placed within the configs folder.
if ( file_exists( dirname( __FILE__ ) . '/configs/' ) ) {
  $files = glob( dirname( __FILE__ ) . '/configs/*.php' );
  if ( ! empty( $files ) ) {
    foreach ( $files as $file ) {
      include $file;
    }
  }
}
if ( ! function_exists( 'redux_register_custom_extension_loader' ) ) {
  function redux_register_custom_extension_loader( $ReduxFramework ) {

    $path = dirname( __FILE__ ) . '/extensions/';
    $folders = scandir( $path, 1 );
    foreach ( $folders as $folder ) {
      if ( $folder === '.' or $folder === '..' or ! is_dir( $path . $folder ) ) {
        continue;
      }
      $extension_class = 'ReduxFramework_Extension_' . $folder;
      if ( ! class_exists( $extension_class ) ) {
        // In case you wanted override your override, hah.
        $class_file = $path . $folder . '/extension_' . $folder . '.php';
        $class_file = apply_filters( 'redux/extension/' . $ReduxFramework->args[ 'opt_name' ] . '/' . $folder, $class_file );
        if ( $class_file ) {
          require_once( $class_file );
        }
      }
      if ( ! isset( $ReduxFramework->extensions[ $folder ] ) ) {
        $ReduxFramework->extensions[ $folder ] = new $extension_class( $ReduxFramework );
      }
    }
  }

  add_action( "redux/extensions/{$domain}_data/before", 'redux_register_custom_extension_loader', 0 );
}