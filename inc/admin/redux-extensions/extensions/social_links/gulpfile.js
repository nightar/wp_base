"use strict";
//init gulp module
var gulp = require('gulp'),
  // init $ autoload from package.json
  $ = require('gulp-load-plugins')({
    overridePattern: true,
    pattern: ['gulp-*', 'gulp.*', '@*/gulp{-,.}*', 'browser-sync', 'imagemin-*'],
  }),
  autoprefixer = require('autoprefixer'),
  combineMq = require('css-mqpacker'),
  // init reload function
  reload = $.browserSync.reload,
  //setup project paths
  path = {
    // build paths
    build: {
      js: 'social_links/',
      css: 'social_links/',
      img: 'social_links/images/',
    },
    // source paths
    source: {
      js: 'source/js/*.js',
      style: 'source/scss/*.scss',
      img: 'source/images/**/*.*',
    },
    //which files are watching
    watch: {
      php: '/**/*.php',
      js: 'source/**/*.js',
      style: 'source/scss/**/*.scss',
      img: 'source/images/**/*.*',
      fonts: 'fonts/**/*.*'
    },
  },
  // server setups
  config = {
    proxy: "http://wp.test",
    host: 'wp.test',
    open: 'external',
  },
  onError = function (err) {
    $.notify.onError({
      title: "Gulp",
      subtitle: "Failure!",
      message: "Error: <%= error.message %>",
      sound: "Beep"
    })(err);
    this.emit('end');
  };


gulp.task('js:build', function () {
  return gulp.src(path.source.js)
    .pipe($.plumber({errorHandler: onError}))
    .pipe($.newer(path.build.js))
    .pipe($.rigger())
    .pipe($.sourcemaps.init())
    .pipe($.uglify())
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
  var plugins = [
    autoprefixer({
      grd: true,
    }),
    combineMq({
      sort: true
    }),
  ]
  return gulp.src(path.source.style)
    .pipe($.plumber({errorHandler: onError}))
    .pipe($.sourcemaps.init())
    .pipe($.sass({outputStyle: 'compressed'})) //nested, expanded, compact, compressed
    .on("error", $.sass.logError)
    .pipe($.postcss(plugins))
    .pipe($.sourcemaps.write('.'))
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
  return gulp.src(path.source.img)
    .pipe($.plumber({errorHandler: onError}))
    .pipe($.newer(path.build.img))
    .pipe($.image({
      pngquant: true,
      optipng: false,
      zopflipng: true,
      jpegRecompress: false,
      mozjpeg: true,
      guetzli: false,
      gifsicle: true,
      svgo: true,
      concurrent: 10,
      quiet: true // defaults to false
    }))
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({stream: true}))
});

gulp.task('build', gulp.parallel([
  'js:build',
  'style:build',
  'image:build'
]));

gulp.task('watch', function () {
  // gulp.watch(path.watch.php, gulp.parallel(reload));
   gulp.watch(path.watch.fonts, gulp.parallel(reload));
   gulp.watch(path.watch.style, gulp.parallel('style:build'));
   gulp.watch(path.watch.js, gulp.parallel('js:build'));
   gulp.watch(path.watch.img, gulp.parallel('image:build'));
});

gulp.task('webserver', function () {
  $.browserSync(config);
});


gulp.task('default', gulp.series('build', gulp.parallel('webserver', 'watch')));