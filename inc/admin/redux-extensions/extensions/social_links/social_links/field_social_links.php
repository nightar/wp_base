<?php
/**
 * Redux Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 * Redux Framework is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Redux Framework. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package     ReduxFramework
 * @author      Dovy Paukstys
 * @version     3.1.5
 */

// Exit if accessed directly
if( ! defined( 'ABSPATH' ) )
  exit;

// Don't duplicate me!
if( ! class_exists( 'ReduxFramework_social_links' ) ) {

  /**
   * Main ReduxFramework_social_links class
   *
   * @since       1.0.0
   */
  class ReduxFramework_social_links {

    private $icons_json;

    /**
     * Field Constructor.
     * Required - must call the parent constructor, then assign field and value to vars, and obviously call the render field function
     *
     * @since       1.0.0
     * @access      public
     * @return      void
     */
    function __construct( $field = [], $value = '', $parent ) {

      $this->parent = $parent;
      $this->field  = $field;
      $this->value  = $value;

      if( empty( $this->extension_dir ) ) {
        $this->extension_dir = trailingslashit( str_replace( '\\', '/', dirname( __FILE__ ) ) );
        $this->extension_url = site_url( str_replace( trailingslashit( str_replace( '\\', '/', ABSPATH ) ), '', $this->extension_dir ) );
      }

      // Set default args for this field to avoid bad indexes. Change this to anything you use.
      $defaults    = [
        'options'          => [],
        'stylesheet'       => '',
        'output'           => true,
        'enqueue'          => true,
        'enqueue_frontend' => true,
      ];
      $this->field = wp_parse_args( $this->field, $defaults );

      $this->icons_json = file_exists( $this->extension_dir . 'icomoon/selection.json' ) ? file_get_contents( $this->extension_dir . 'icomoon/selection.json' ) : '';
      if( $this->icons_json ) {
        $this->icons_json = json_decode( $this->icons_json );
      }

    }

    /**
     * Field Render Function.
     * Takes the vars and outputs the HTML for the field in the settings
     *
     * @since       1.0.0
     * @access      public
     * @return      void
     */
    public function render() {

      $options = $this->getIconsOptions();
      ?>
      <div class="social-links-holder">
        <div class="list-holder">
          <?php if( $this->value ): ?>
            <?php foreach( $this->value as $key => $v ):
              $v_icon = isset( $v[ 'icon' ] ) ? $v[ 'icon' ] : '';
              $v_url = isset( $v[ 'url' ] ) ? $v[ 'url' ] : '';
              ?>
              <div class="social-link-container" data-loop="<?php echo $key; ?>">
                <select class="select-item" data-placeholder="<?php _e( 'Select the icon', 'field_social_link' ) ?>"
                        name="<?php echo $this->field[ 'name' ] . "[{$key}][icon]" ?>"
                        id="<?php echo $this->field[ 'id' ] . "-{$key}-icon" ?>">
                  <option value=""></option>
                  <?php foreach( $options as $value => $label ): ?>
                    <option data-icon="<?php echo $value; ?>"
                            value="<?php echo $value; ?>"<?php selected( $value, $v_icon ) ?>><?php echo $label; ?></option>
                  <?php endforeach; ?>
                </select>
                <input type="url" class="url-input" id="<?php echo $this->field[ 'id' ] . "-{$key}-url" ?>"
                       name="<?php echo $this->field[ 'name' ] . "[{$key}][url]" ?>" placeholder="http://example.com/"
                       value="<?php echo $v_url; ?>">
                <a href="javascript:;" class="action-link delete"><span class="dashicons dashicons-no">&nbsp;</span></a>
              </div>
            <?php endforeach; ?>
          <?php endif; ?>
        </div>
        <a href="javascript:;" class="action-link add"><span class="dashicons dashicons-plus">&nbsp;</span></a>
        <script type="text/template" class="social-link-template">
          <div class="social-link-container" data-loop="{?}">
            <select class="select-item" data-placeholder="<?php _e( 'Select the icon', 'field_social_link' ) ?>"
                    name="<?php echo $this->field[ 'name' ] . "[{?}][icon]" ?>"
                    id="<?php echo $this->field[ 'id' ] . "-{?}-icon" ?>">
              <option value=""></option>
              <?php foreach( $options as $value => $label ): ?>
                <option data-icon="<?php echo $value; ?>"
                        value="<?php echo $value; ?>"><?php echo $label; ?></option>
              <?php endforeach; ?>
            </select>
            <input type="url" class="url-input" id="<?php echo $this->field[ 'id' ] . "-{?}-url" ?>"
                   name="<?php echo $this->field[ 'name' ] . "[{?}][url]" ?>" placeholder="http://example.com/"
                   value="">
            <a href="javascript:;" class="action-link delete"><span class="dashicons dashicons-no">&nbsp;</span></a>
          </div>
        </script>
      </div>
      <?php
    }

    /**
     * Get select options data
     *
     * @since       1.0.0
     * @access      public
     * @return array
     */
    private function getIconsOptions() {

      $options = [];
      if( $this->icons_json && $this->icons_json->icons ) {
        foreach( $this->icons_json->icons as $icon ) {
          $path = '';
          foreach( $icon->icon->paths as $path_item ) {
            $path .= "<path d=\"{$path_item}\" />";
          }
          $options[ 'icon icon-' . $icon->properties->name ] = "{$icon->properties->name}";
        }
      }

      return $options;
    }

    /**
     * Enqueue Function.
     * If this field requires any scripts, or css define this function and register/enqueue the scripts/css
     *
     * @since       1.0.0
     * @access      public
     * @return      void
     */
    public function enqueue() {

      wp_enqueue_script( 'redux-field-icon-select-js', $this->extension_url . 'field_social_links.js', [ 'jquery' ], false, true );
      wp_enqueue_style( 'redux-field-icons-css', $this->extension_url . '/icomoon/style.css', [], false );
      wp_enqueue_style( 'redux-field-icon-select-css', $this->extension_url . 'field_social_links.css', [], false );

    }

  }
}
