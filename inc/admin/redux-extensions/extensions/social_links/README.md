# ReduxFramework Social Links
Social link field for WordPress Redux Framework

![GitHub Logo](https://gitlab.com/nightar/social_links/raw/master/social_links.png)

Social links field for redux framework 

#### Learn About Redux Extensions
https://docs.reduxframework.com/extensions/using-extensions/

https://github.com/reduxframework/redux-extensions-loader

#### Install social links field

#####Composer

1. Install [composer](https://getcomposer.org/doc/00-intro.md)
2. Open console
3. Go to Redux framework extensions folder
    ```cmd
    cd ..redux-extensions/extensions
    ``` 
4. Run command
    ```cmd
    composer create-project nightar/social_links
    ``` 
5. Include extensions used [redux-loader](https://github.com/reduxframework/redux-extensions-loader)
6. create field
```php
 [
    'title'      => __( 'Section title', 'domain' ),
    'id'         => 'section-id',
    'subsection' => true, //or 'flase' if it is the main section
    'desc'       => __( 'Section description', 'domain' ),
    'fields'     => [
      [
        'id'    => 'socials_field_id',
        'type'  => 'social_links',
        'title' => __( 'Social links title', 'domain' ),
      ],
    ]
  ],
```

#####Manual

1. Copy project from [git](https://gitlab.com/nightar/social_links) to extensions folder
2. Doing steps 5,6 from composer install method 

Lets Enjoy!!!
