<?php

namespace theme\Admin;

use theme\Classes\Base;
use theme\Theme;
use theme\Helper;
use Redux;

class Admin extends Base {

  private $options_name = '';

  public function init() {

    $this->options_name = Theme::domain() . '_data';

    if ( file_exists( dirname( __FILE__ ) . '/tgm/tgm-init.php' ) ) {
      require_once dirname( __FILE__ ) . '/tgm/tgm-init.php';
    }

    // Load the embedded Redux Framework
    if ( file_exists( dirname( __FILE__ ) . '/redux-framework/framework.php' ) ) {
      require_once dirname( __FILE__ ) . '/redux-framework/framework.php';
    }

    $this->optionInit();

    // Load Redux extensions
    if ( file_exists( dirname( __FILE__ ) . '/redux-extensions/extensions-init.php' ) ) {
      require_once dirname( __FILE__ ) . '/redux-extensions/extensions-init.php';
    }

    add_filter( "redux/{$this->options_name}/aURL_filter", [$this, 'removeAUrl']);

  }

  public function removeAUrl( $data ) {
    return null;
  }

  public static function getOptionsName() {
    return self::instance()->options_name;
  }

  protected function optionInit() {

    if ( ! class_exists( 'Redux' ) ) {
      return;
    }

    Redux::setArgs( $this->options_name, $this->getPageArgs() );

    $options = $this->getPageOptions();

    if ( $options ) {
      Redux::setSections( $this->options_name, $options);
    }
  }

  protected function getPageOptions() {
    $options = [];
    if ( Helper::getThemeDirectory( '/setup/admin/options.php') ) {
      include_once  Helper::getThemeDirectory( '/setup/admin/options.php');
    }
    $options = apply_filters( 'theme-settings-page-options', $options);
    return $options;
  }

  protected function getPageArgs() {
    $args = [];
    if ( Helper::getThemeDirectory( '/setup/admin/page.php') ) {
      include_once  Helper::getThemeDirectory( '/setup/admin/page.php');
    }
    $args = apply_filters( 'theme-settings-page-data', $args);
    return $args;
  }
}