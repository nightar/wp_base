<?php
use theme\Helper;
/**
 * TGM Init Class
 */
include_once ('class-tgm-plugin-activation.php');

function theme_data_register_required_plugins() {
  $plugins = [];
  if ( Helper::getThemeDirectory( '/setup/plugins.php') ) {
    include_once  Helper::getThemeDirectory( '/setup/plugins.php');
  }

	$config = array(
		'domain'       		=> 'redux-framework',         	// Text domain - likely want to be the same as your theme.
		'default_path' 		=> get_theme_file_path( 'plugins/' ),                         	// Default absolute path to pre-packaged plugins
                'parent_slug'           => 'plugins.php',
                'capability'            => 'manage_options',
		'menu'         		=> 'install-required-plugins', 	// Menu slug
		'has_notices'      	=> true,                       	// Show admin notices or not
		'is_automatic'    	=> true,					   	// Automatically activate plugins after installation or not
	);

	tgmpa( $plugins, $config );

}
add_action( 'tgmpa_register', 'theme_data_register_required_plugins' );