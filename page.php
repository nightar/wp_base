<?php

use theme\Helper;

get_header();

$content_attributes = [
  'id' => 'content',
];
?>
<?php while( have_posts() ): the_post(); ?>
  <div<?php echo Helper::getAttrString( $content_attributes ); ?>>
    <?php do_action( 'location-blocks/single/content' ); ?>
  </div>
<?php endwhile; ?>
<?php get_sidebar(); ?>
<?php get_footer(); ?>