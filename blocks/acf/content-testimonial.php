<?php
/**
 * Created by PhpStorm.
 * User: syallin
 * Date: 04.03.2019
 * Time: 12:37
 */
$additional_classes = $block['className'];
$additional_classes = preg_split("|\s|", $additional_classes);
$additional_classes = array_map( 'sanitize_html_class', $additional_classes);
$additional_classes = implode( ' ', $additional_classes);
$block_id = sanitize_html_class( $block['customId'] );
?>