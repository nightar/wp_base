<?php

use theme\Helper;
use theme\menu\walker\NavMenu;

?>
<footer id="footer" class="footer">
  <?php if( is_active_sidebar( 'footer-sidebar' ) ) : ?>
    <section class="bottom-section">
      <?php if( is_active_sidebar( 'footer-sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'footer-sidebar' ); ?>
      <?php endif; ?>
    </section>
  <?php endif; ?>
  <div class="holder">
    <?php if( $logo = Helper::getLogo( 'footer' ) ) : ?>
      <div class="logo footer-logo" itemscope itemtype="http://schema.org/Brand">
        <?php echo $logo; ?>
      </div>
    <?php endif; ?>
    <?php if( has_nav_menu( 'footer' ) ): ?>
      <?php wp_nav_menu( [
        'container'       => 'div',
        'container_class' => 'nav-section',
        'theme_location'  => 'footer',
        'menu_id'         => 'navigation',
        'menu_class'      => 'navigation',
        'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'walker'          => new NavMenu,
      ] ); ?>

    <?php endif; ?>
  </div>
  <?php if( $copyright = Helper::getOption( 'copyright' ) ): ?>
    <div class="copyright">
      <?php echo wpautop( $copyright ); ?>
    </div>
  <?php endif; ?>
</footer>