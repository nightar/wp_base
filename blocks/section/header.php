<?php

use theme\Helper;
use theme\menu\walker\NavMenu;

/**
 * Heade navigation section
 */
$attributes = [
  'class' => [
    'section-header',
  ],
];
?>
<?php if( $logo = Helper::getLogo() ) : ?>
  <div class="logo" itemscope itemtype="http://schema.org/Brand">
    <?php echo $logo; ?>
  </div>
<?php endif; ?>
<?php if( has_nav_menu( 'primary' ) ) : ?>
  <?php
  wp_nav_menu( [
    'container'       => 'nav',
    'container_class' => 'navbar',
    'theme_location'  => 'primary',
    'menu_id'         => 'navigation',
    'menu_class'      => 'navbar-nav',
    'items_wrap'      => '<a class="menu-opener" href="javascript:;">&nbsp;</a><ul id="%1$s" class="%2$s">%3$s</ul>',
    //'walker'          => new NavMenu,
  ] ); ?>
<?php endif; ?>
<?php if( $socials = Helper::getOption( 'socials' ) ): ?>
  <ul class="socials-list">
    <?php foreach( $socials as $item ): ?>
      <?php if( $item[ 'icon' ] && $item[ 'url' ] ): ?>
        <li><a href="<?php echo esc_url( $item[ 'url' ] ) ?>"><i
              class="<?php echo $item[ 'icon' ]; ?>">&nbsp;</i></a></li>
      <?php endif; ?>
    <?php endforeach; ?>
  </ul>
<?php endif; ?>