<?php
use theme\Theme;
the_posts_pagination( array(
  'prev_text' => __( 'Previous page', Theme::domain() ),
  'next_text' => __( 'Next page', Theme::domain() ),
) ); ?>