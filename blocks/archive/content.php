<?php
use theme\Helper;
use theme\Classes\CPT;
?><div <?php post_class( 'item' ); ?> id="post-<?php the_ID(); ?>">
  <div class="over">
  	<?php if( has_post_thumbnail() ): ?>
  	  <div class="image-holder">
  	    <?php the_post_thumbnail( 'thumbnail_441x441' ); ?>
  	  </div>
  	<?php endif; ?>
  	<div class="content">
  	  <?php the_title( '<h3><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
  	  <?php echo CPT::getExcerptLimit( 144 ); ?>
  	</div>
  </div>
</div>
