<?php
$page_id = get_queried_object_id();
$thumbnail_id = has_post_thumbnail( $page_id ) ? get_post_thumbnail_id( $page_id ) : false;
?>
<section class="hero simple">
  <div class="over-bg" <?php if ( $thumbnail_id )
    echo ' style="background-image: url(' . wp_get_attachment_image_url( $thumbnail_id, 'full' ) . ')"'; ?>></div>
  <div class="holder">
    <div class="slide-text">
      <h2><?php echo get_the_title( $page_id ); ?></h2>
    </div>
  </div>
</section>