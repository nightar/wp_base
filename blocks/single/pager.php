<?php
$next = get_next_post();
$prev = get_previous_post();
if( $next or $prev ): ?>
  <?php if( $next ) : ?>
    <a href="<?php echo get_permalink( $next ) ?>" class="btn btn-next"><i class="icon icon-arrow-right"></i></a>
  <?php endif; ?>
  <?php if( $prev ) : ?>
    <a href="<?php echo get_permalink( $prev ) ?>" class="btn btn-prev"><i class="icon icon-arrow-left"></i></a>
  <?php endif; ?>
<?php endif; ?>
