<?php

use theme\Theme;

$intro = get_field( 'intro_text' );
?>
<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
  <?php the_title( '<h1>', '</h1>' ); ?>
  <?php if ( trim( get_the_content() ) ): ?>
    <div class="content">
      <?php the_content(); ?>
    </div>
  <?php endif; ?>
  <?php wp_link_pages(); ?>
  <?php edit_post_link( __( 'Edit', Theme::domain() ) ); ?>
</div>
