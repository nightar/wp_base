<?php

use theme\Theme;

?>
<?php if( trim( get_the_content() ) ): ?>
  <div class="content">
    <?php the_content(); ?>
  </div>
<?php endif; ?>
<?php edit_post_link( __( 'Edit', Theme::domain() ) ); ?>
<?php wp_link_pages(); ?>
<?php comments_template(); ?>
